import styles from "./FavPage.module.scss";
import ShopItem from "../../components/ShopItem";
import ShopItemTable from "../../components/ShopItemTable";
import { useSelector } from "react-redux";
import { useThemeType } from "../../context/themeContext/ThemeContext";

function FavPage() {
  const { themeType } = useThemeType();
  const [goods] = useSelector(({ goods }) => [goods.items, goods.favRerender]);

  return (
    <div>
      {themeType === "card" && (
        <section className={styles.FavPage}>
          {goods.length > 0 &&
            goods.map(
              (e, index) =>
                e.isFav && (
                  <ShopItem
                    {...e}
                    key={e.id}
                    index={index}
                    shop={true}
                    cart={false}
                  />
                )
            )}
          {goods.filter(({ isFav }) => isFav).length > 0 || (
            <h3>You don't have any items in favorites</h3>
          )}
        </section>
      )}
      {themeType === "table" && (
        <section style={{ display: "flex", justifyContent: "center" }}>
          <table>
            <caption>Favorites Products</caption>
            <tbody>
            {goods.length > 0 &&
            goods.map(
              (e, index) =>
                e.isFav && (
                  <ShopItemTable
                    {...e}
                    key={e.id}
                    index={index}
                    shop={true}
                    cart={false}
                  />
                )
            )}
            </tbody>
          </table>
          {goods.filter(({ isFav }) => isFav).length > 0 || (
            <h3>You don't have any items in favorites</h3>
          )}
        </section>
      )}
    </div>
  );
}

export default FavPage;
