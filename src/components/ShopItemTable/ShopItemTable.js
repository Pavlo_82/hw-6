import React from 'react';
import styles from './ShopItem.module.scss';
import { ReactComponent as FavSvgTrue } from './add-fav-true.svg';
import { ReactComponent as FavSvgFalse } from './add-fav-false.svg';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { showModal, toggleFavGoods, addToCart, deleteFromCart } from '../../store/actionCreators';

function ShopItemTable(props) {
    const dispatch = useDispatch();
    const { shop, cart, name, price, image, vendorCode, color, id, backgroundColor, index, inCartCount } = props;
    const isFav = useSelector(store => store.goods.items[index].isFav);

    return (
        <tr className={styles.ShopItemTable} style={{ backgroundColor }}>
            <td><img src={image} alt={name}></img></td>
            <td onClick={() => {
                dispatch(toggleFavGoods(id));
            }}>
                {isFav ? <FavSvgTrue /> : <FavSvgFalse />}
            </td>
            <td><h3>{name}</h3></td>
            <td>
                Color: {color}
            </td>
            <td>vendorCode: {vendorCode}</td>
            <td><span>{price} ₴</span>
                <p>{inCartCount ? `In cart: ${inCartCount} pcs.` : ''}&nbsp;</p></td>
            <td>
                {shop && <button onClick={() => {
                    dispatch(showModal({
                        title: 'Add to cart?', text: 'Are you sure want to add this to cart?', actionFn: () => {
                            dispatch(addToCart(id));
                        }
                    }));
                }}>Add to cart</button>}
            </td>
            <td>{cart && <button onClick={() => {
                dispatch(showModal({
                    title: 'Remove from cart?', text: `Are you sure want to remove this vendorCode (${vendorCode}) from cart?`, actionFn: () => {
                        dispatch(deleteFromCart(id));
                    }
                }));
            }}>Remove from cart</button>}</td>

        </tr>
    );
}

ShopItemTable.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    image: PropTypes.string.isRequired,
    vendorCode: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,

    backgroundColor: PropTypes.string,
    isFav: PropTypes.bool,
}

ShopItemTable.defaultProps = {
    removeCart: undefined,
    addCart: undefined,
    backgroundColor: 'lightgrey',
    isFav: false
}

export default ShopItemTable;